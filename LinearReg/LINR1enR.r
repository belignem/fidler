####### R�gression lin�aire avec r�solution directe ####### 


## Etape 1 : import et initialisation

library(ggplot2)



##  Etape 2 : Cr�ation d'un ensemble de points


# ---- Param�tres
nb    <- 100     # Nombre de points
xmin  <- 0       # Distribution / x
xmax  <- 10
a     <- 4       # Distribution / y
b     <- 2       # y= a.x + b (+ bruit)
noise <- 7       # bruit

theta <- c(a,b)

# ---- Vecteur X  (1,x) x nb
#      la premiere colonne est a 1 afin que X.theta <=> 1.b + x.a

Xc1 <- matrix(rep(1, nb), ncol = 1)
Xc2 <- matrix(runif(nb, min = xmin, max = xmax), ncol = 1)
X <- cbind(Xc1, Xc2)

# ---- Noise
# N = np.random.uniform(-noise,noise,(nb,1))
N <- noise * rnorm(nb)

# ---- Vecteur Y
Y <- X %*% theta + N

# Afficher ce nuage de points
print("Etape 2 : nuage initial de points")

ggplot(data = data.frame(x = X[,2], y = Y))+
  geom_point(aes(x = x, y = y))+
  xlab("x axis") +
  ylab("y axis")



##  Etape 3 : calcul direct de l'�quation normale

theta_hat <- solve(t(X) %*% X) %*% t(X) %*% Y
cat("Theta:",theta,"theta hat:",theta_hat, sep="\n")

Xd <- matrix(c(1, xmin, 1, xmax), nrow = 2, byrow = TRUE)
Yd <- Xd %*% theta_hat

print("Etape 3 : nuage initial de points et r�sulat obtenu")

ggplot() + 
  geom_point(aes(X[, 2], Y)) + 
  geom_line(aes(Xd[, 2], Yd)) + 
  xlab("x axis") + 
  ylab("y axis")


