# FidleR

## D'où vient ce projet ?

Pour une présentation aux [Vendredis quanti](https://quantigre.hypotheses.org/) le 3 février 2023 intitulée : "Réseaux de neurones : principes et applications à des corpus textuels", une première idée a germé : celle de mettre en place un projet collaboratif pour transformer une partie des notebooks Python de la formation [Fidle](https://gricad-gitlab.univ-grenoble-alpes.fr/talks/fidle) 2022/2023 dans la langage R. L'objectif est de fournir à une communauté qui utilise majoritairement ce langage une porte d'entrée et un espace collaboratif sur cette formation dédiée aux réseaux de neurones sans passer par la case "Apprentissage du langage Python".

Après avoir traduit les premiers notebooks, une deuxième idée à germé : celle de pouvoir ajouter quelques notebooks complémentaires. Cela permet d'ouvrir le projet au-delà de la simple reproduction d'une formation existante et de reflechir à des ajouts pédagogiques. Les nouveaux Notebooks sont signalés par le suffixe "NV" ci-dessous (ex:GRAD2NV). Il peut être stratégique si vous débutez de commencer par ces notebooks qui prennent le temps de développer certains détails.

Pour chaque notebook, une version statique et dynamique sont proposées ainsi qu'un fichier R.

## Notebooks et fichiers disponibles :

### Régression linéaire et logistique
- **LINR1 : Regression linéraire avec résolution directe** - [Notebook statique](LinearReg/LINR1enR.ipynb), [Notebook Colab](https://colab.research.google.com/drive/185lc1Hc0HTL7xbG3W9n8HaClBvVUKIcX), [Fichier R](LinearReg/LINR1enR.r)  
- **GRAD1 : Regression linéaire avec descente de gradient** - [Notebook Colab](https://colab.research.google.com/drive/1M_YO2UTPOPOzwwkySWDssh_w9QrjdXVu)
- **GRAD2NV : Idem GRAD1 en plus détaillé** - Pour l'instant qu'un début en ColabNotebook [Python](https://colab.research.google.com/drive/1VrpyHbItfYxU7iFQBag8yezd2XGBunrB)
- **POLR1 : Problème de la complexité** - [Notebook Colab](https://colab.research.google.com/drive/1Yc8hVjYK9sBSklR28ctVsnKYUps05C1N)
- **LOGR1 : Une simple régression logistique** - [Notebook Colab](https://colab.research.google.com/drive/1Mq1LkTNXLNq9HBkp2fgvQPk0n-ub33oY#scrollTo=gUCEoUuiMpcV)

### Modèle du Perceptron

- **LOGR1 : Ex historique du Perceptron (1957) sur le jeu de données IRIS(1936)** - 

## Contribution

Si vous voulez contribuer à ce projet, corrections ou ajouts, n'hésitez pas à m'écrire :
beligne(point)max(arobase + suffixe gmail.com)

## Auteur

Max Beligne, Plateforme Universitaire de Données Grenoble-Alpes
.
